﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManager;
    public Ball ball;
    public Text BlueScore;
    public Text RedScore;
    public Text WinnerLabel;
    public GameObject ExitButton;
    public static int targetScore=5;

    public static Vector2 bottomLeft;
    public static Vector2 topRight;
    public static Vector2 edgeVector;
    public static int redScore = 0;
    public static int blueScore = 0;
    // Start is called before the first frame update
    private void Awake()
    {
        if (GameManager.gameManager == null) { GameManager.gameManager = this; }
        else if (GameManager.gameManager != this) { Destroy(GameManager.gameManager.gameObject); GameManager.gameManager = this; }
    }

    void Start()
    {
        edgeVector = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
        topRight = new Vector2(edgeVector.x, edgeVector.y);
        bottomLeft = new Vector2(-edgeVector.x, -edgeVector.y);
        Instantiate(ball);
    }

    // Update is called once per frame
    void Update()
    {
        BlueScore.text = blueScore.ToString();
        RedScore.text = redScore.ToString();
    }

    public void ShowExitButton(bool show)
    {
        GameManager.gameManager.ExitButton.SetActive(show);
    }

    public void OnExitButtonClick()
    {
        StartCoroutine(LeaveRoom());
    }
    IEnumerator LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        while (PhotonNetwork.InRoom)
            yield return null;
        PhotonNetwork.LoadLevel(MultiplayerSetting.multiplayerSetting.menuScene);
    }
}

