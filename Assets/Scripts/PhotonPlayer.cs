﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PhotonPlayer : MonoBehaviour
{
    private PhotonView PView;

    public GameObject myAvatar;
    // Start is called before the first frame update
    void Start()
    {
        PView = GetComponent<PhotonView>();
        if(PView.IsMine)
        {
            myAvatar=PhotonNetwork.Instantiate(Path.Combine("Prefabs", "Paddle"), GameSetup.gameSetup.spawnPoints[PhotonNetwork.IsMasterClient ? 0 : 1].position, Quaternion.identity, 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
