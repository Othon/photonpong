﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private PhotonView PView;
    public float speed;
    private float height;
    private SpriteRenderer sprite;
    // Start is called before the first frame update
    void Start()
    {
        PView=GetComponent<PhotonView>();
        height = transform.localScale.y;
        sprite = GetComponent<SpriteRenderer>();
        if(PhotonNetwork.IsMasterClient)
        {
            sprite.color = PView.IsMine ? Color.red : Color.blue;
        }
        else
        {
            sprite.color = PView.IsMine ? Color.blue : Color.red;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Avoid moving the other player's paddle.
        if (PView.IsMine) {
            //Move on input.
            float move = Input.GetAxis("Vertical") * speed * Time.deltaTime;
            //Restrict movement to screen broders
            if (transform.position.y >= GameManager.topRight.y - height / 2 && move > 0) { move = 0; }
            if (transform.position.y <= GameManager.bottomLeft.y + height / 2 && move < 0) { move = 0; }
            //Move
            transform.Translate(move * Vector2.up);
        }
    }
}
