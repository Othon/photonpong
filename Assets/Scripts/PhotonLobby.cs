﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotonLobby : MonoBehaviourPunCallbacks
{
    public static PhotonLobby lobby;
    RoomInfo[] rooms;

    public GameObject ConnectButton;
    public GameObject CancelButton;
    public GameObject ExitButton;
    public GameObject OfflineBanner;
    public Text MessageLabel;

    private void Awake()
    {
        lobby = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
        MessageLabel.text = "Connecting...";
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to master server.");
        PhotonNetwork.AutomaticallySyncScene=true;
        ConnectButton.SetActive(true);
        MessageLabel.text = "";
    }

    public void OnConnectButtonClick()
    {
        ConnectButton.SetActive(false);
        CancelButton.SetActive(true);
        PhotonNetwork.JoinRandomRoom();
        MessageLabel.text = "Searching room...";
    }

    public override void OnJoinRandomFailed(short returnCode,string message)
    {
        Debug.Log("Not rooms available at this time.");
        CreateRoom();
    }

    void CreateRoom()
    {
        Debug.Log("Creating new room.");
        int randomRoomName = Random.Range(0, 10);
        RoomOptions roomOptions = new RoomOptions()
        {
            IsVisible = true,
            IsOpen = true,
            MaxPlayers = (byte)MultiplayerSetting.multiplayerSetting.maxPlayers
        };
        PhotonNetwork.CreateRoom(randomRoomName.ToString(), roomOptions);
        MessageLabel.text = "Creating room...";
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Room name already in use.");
        CreateRoom();
    }

    public void OnCancelButtonClick()
    {
        Debug.Log("Connection closed.");
        CancelButton.SetActive(false);
        ConnectButton.SetActive(true);
        PhotonNetwork.LeaveRoom();
        MessageLabel.text = "";
    }

    public void OnExitButtonClick()
    {
        Debug.Log("Exit room.");
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LoadLevel(MultiplayerSetting.multiplayerSetting.menuScene);
    }
}
