﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField]
    float speed;
    float radius;
    Vector2 direction;
    // Start is called before the first frame update
    void Start()
    {
        direction = Vector2.one.normalized;
        radius = transform.localScale.x / 2;
        speed = 9f;
    }

    // Update is called once per frame
    void Update()
    {
        //Move ball
        transform.Translate(direction * speed * Time.deltaTime);
        //Constraint to upper and lower boders
        if (transform.position.y < GameManager.bottomLeft.y + radius && direction.y<0)
        {
            direction.y = -direction.y;
        }
        if (transform.position.y > GameManager.topRight.y - radius && direction.y > 0)
        {
            direction.y = -direction.y;
        }
        //Game over when passing left or right border

        if (transform.position.x < GameManager.bottomLeft.x + radius && direction.x < 0)
        {
            GameManager.blueScore++;

            if (GameManager.blueScore >= GameManager.targetScore)
            {
                Debug.Log("Blue team wins");
                //TODO:finish game properly
                GameManager.gameManager.ShowExitButton(true);
                GameManager.gameManager.WinnerLabel.text = "Blue Team Wins";
                transform.gameObject.SetActive(false);
            }
            else
            {
                transform.position = new Vector2(0, transform.position.y);
                speed = 9f;
                direction.x = -direction.x;
            }
        }
        if (transform.position.x > GameManager.topRight.x - radius && direction.x > 0)
        {
            GameManager.redScore++;
            if (GameManager.redScore >= GameManager.targetScore)
            {
                Debug.Log("Red team wins");
                //TODO:finish game properly
                GameManager.gameManager.ShowExitButton(true);
                GameManager.gameManager.WinnerLabel.text = "Red Team Wins";
                transform.gameObject.SetActive(false);
            }
            else
            {
                transform.position = new Vector2(0,transform.position.y);
                speed = 9f;
                direction.x = -direction.x;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag=="Paddle")
        {
            direction.x = -direction.x;
            speed = speed + 0.45f;
        }
    }

}
