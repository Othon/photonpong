PhotonPong
This is a simple pong game used to test the online comunication quality provided by the Photon pluggin.

Known issues:

- Error upon exit from a room (online session with other player) even though the game still works as expected.
- Entering a room after exit from a previous one causes the game to continue the game from where it left off.
- Player colors are not correctly displayed in the screen of the other player.
- Due to lag the game might display different results for each client in the same session. Testing has resulted in players seeing different final scores for the same session.
- 